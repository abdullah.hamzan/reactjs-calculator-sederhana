import React from 'react'
import '../styles/Button.css'
import { Button } from '@material-ui/core';


const ButtonComponent = ({name,onClick,color}) => {
    return (
        
        // <button onClick={onClick} name={name} style={{ width:'40px',height:'20px',color:'black' }} className="button-primary"  />
        <Button variant="contained" color={color} onClick={onClick}>
        {name}
      </Button>

       
    )
}

export default ButtonComponent
