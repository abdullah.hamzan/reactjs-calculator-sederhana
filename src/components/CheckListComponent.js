
import React from 'react'
import Checkbox from '@material-ui/core/Checkbox';

const CheckListComponent = ({checked,onChange}) => {
    return (
       <Checkbox
        checked={checked}
        onChange={onChange}
        inputProps={{ 'aria-label': 'primary checkbox' }}
      />
            
        
    )
}

export default CheckListComponent
